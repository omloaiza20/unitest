import pytest

from division import division

def test_div_normal():
    calculator = division()

    result = calculator.div(13, 2)

    assert result == 6.5


def test_div_por_cero():
    calculator = division()

    result = calculator.div(5, 0)

    assert result == print("no se puede dividir por cero")